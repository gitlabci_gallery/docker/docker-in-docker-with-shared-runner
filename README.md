# docker-in-docker with a shared runner

Example of docker in docker usage with a shared runner.

To run this kind of project you first have to check that:

1. The *CI/CD* option is switched on in [*Settings - General - Visibility, project features, permissions*](https://gitlab.inria.fr/gitlabci_gallery/docker/docker-in-docker/edit).
2. The *Container registry* option is switched on in [*Settings - General - Visibility, project features, permissions*](https://gitlab.inria.fr/gitlabci_gallery/docker/docker-in-docker/edit).
3. The *Enable shared runners for this project* is switched on in [*Settings - CI/CD - Runners*](https://gitlab.inria.fr/gitlabci_gallery/docker/docker-in-docker/-/settings/ci_cd).

The *.gitlab-ci.yml* file contains two jobs/stages that will be executed sequentially. Both are executed on a shared runner.

The first one does load the docker:20.10.16 image that has a valid docker daemon. This daemon is used to build the docker image with the given Dockerfile. This image is then stored in the [*Packages and registries - Container Registry*](https://gitlab.inria.fr/gitlabci_gallery/docker/docker-in-docker/container_registry).

The second one does load the docker image build and registered during the first stage and run a simple command in this container.
